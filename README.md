
![alternativetext](https://bitbucket.org/arifjehoh/note/raw/604322f04c9ad0931ce421acedf3c6b2b2992ebc/img/overlay.png =150x)
# Note
Simple ui but with advanced usage of service and tools to create the app.
This project is built after what I have learned from my internship. 
This is a note-taking app what works with a local database and architecturered with MVVM, android architecture, in mind.


## Build with
* Room, for local database
* MVVM, for structuring the project
* Databinding, for easily get/set values
* LiveData
* Swipeables item


### Screen
![alternativetext](https://bitbucket.org/arifjehoh/note/raw/d5e749081f8928b374712f1ec38cc494b8ad9123/img/0%20%E2%80%93%201.png)
