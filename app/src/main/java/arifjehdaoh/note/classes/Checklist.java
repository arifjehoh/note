package arifjehdaoh.note.classes;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

public class Checklist extends BaseObservable {
    private boolean checked;
    private String text;

    public Checklist(String text) {
        this.text = text;
    }

    public Checklist() {

    }

    @Bindable
    public boolean isChecked() {
        return checked;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
