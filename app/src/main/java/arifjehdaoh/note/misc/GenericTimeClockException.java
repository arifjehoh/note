package arifjehdaoh.note.misc;

public class GenericTimeClockException extends TimeClockException {
    public final Throwable throwable;

    GenericTimeClockException(Throwable throwable) {
        this.throwable = throwable;
    }

    @Override
    public String toString() {
        return throwable.toString();
    }
}
