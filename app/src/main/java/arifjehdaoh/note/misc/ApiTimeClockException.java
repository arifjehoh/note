package arifjehdaoh.note.misc;

public class ApiTimeClockException extends TimeClockException {
    public final int httpStatusCode;
    public final int errorCode;

    public ApiTimeClockException(int httpStatusCode, int errorCode) {
        this.httpStatusCode = httpStatusCode;
        this.errorCode = errorCode;
    }

    @Override
    public String toString() {
        return "NewApiRubyException{"
                + "errorCode=" + errorCode
                + '}';
    }
}
