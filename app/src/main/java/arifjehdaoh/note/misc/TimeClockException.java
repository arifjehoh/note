package arifjehdaoh.note.misc;

import org.json.JSONException;
import retrofit2.Response;

import timber.log.Timber;

import java.io.IOException;

import arifjehdaoh.note.api.entity.ErrorBody;

public class TimeClockException extends Exception {
    TimeClockException() {
    }

    public static TimeClockException unknownError() {
        return new GenericTimeClockException(new Exception());
    }

    public static TimeClockException create(Response<?> response) {
        final String host = response.raw().request().url().host();

        //TODO: Parse the error response here and set

            final okhttp3.ResponseBody errorResponseBody = response.errorBody();
            String errorBodyString = null;
            try {
                errorBodyString = errorResponseBody.string();
                ErrorBody errorBody = ErrorBody.fromString(errorBodyString);
                return new ApiTimeClockException(response.code(),
                        errorBody.errorCode);
            } catch (IOException | JSONException e) {
                Timber.e(new Exception(errorBodyString, e), "RubyException body: %s", errorBodyString);
                return new GenericTimeClockException(e);
            }
    }

    public static TimeClockException create(Throwable throwable) {
        return new GenericTimeClockException(throwable);
    }
}