package arifjehdaoh.note.repository;

import retrofit2.Response;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import arifjehdaoh.note.misc.TimeClockException;

/**
 * A generic class that holds a value with its loading status and possible errors.
 * @param <T> The type of object the resource will contain
 */
public class Resource<T> {

    @NonNull
    public final Status status;

    @Nullable
    public final T data;

    @Nullable
    public final TimeClockException exception;

    private Resource(@NonNull Status status, @Nullable T data, @Nullable TimeClockException exception) {
        this.status = status;
        this.data = data;
        this.exception = exception;
    }

    public static <T> Resource<T> success(@NonNull T data) {
        return new Resource<>(Status.SUCCESS, data, null);
    }

    public static <T> Resource<T> success() {
        return new Resource<>(Status.SUCCESS, null, null);
    }

    public static <T> Resource<T> error(@NonNull Response<?> response) {
        return new Resource<>(Status.ERROR, null, TimeClockException.create(response));
    }

    public static <T> Resource<T> error(@NonNull Throwable throwable) {
        return new Resource<>(Status.ERROR, null, TimeClockException.create(throwable));
    }

    public static <T> Resource<T> error(@NonNull Resource<?> resource) {
        return new Resource<>(Status.ERROR, null, resource.exception);
    }

    public static <T> Resource<T> error() {
        return new Resource<>(Status.ERROR, null, TimeClockException.unknownError());
    }

    public static <T> Resource<T> loading() {
        return new Resource<>(Status.LOADING, null, null);
    }

    /**
     * Status of a resource that is provided to the UI.
     */
    public enum Status {
        SUCCESS,
        ERROR,
        LOADING
    }

}