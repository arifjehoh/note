package arifjehdaoh.note.repository;

import android.os.AsyncTask;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import arifjehdaoh.note.api.entity.NoteEntity;
import arifjehdaoh.note.main.NoteDao;

public class NoteLocalDbRepository {
    private NoteDao noteDao;

    @Inject
    public NoteLocalDbRepository(NoteDao noteDao) {
        this.noteDao = noteDao;
    }

    public LiveData<List<NoteEntity>> getNotes() {
        return noteDao.getNotes();
    }

    public LiveData<NoteEntity> getNote(int id) {
        return noteDao.getNote(id);
    }

    public void delete(NoteEntity note) {
        new deleteAsyncTask(noteDao).execute(note);
    }

    public void insertNote(NoteEntity entity) {
        new insertAsyncTask(noteDao).execute(entity);
    }

    public void update(NoteEntity note) {
        new updateAsyncTask(noteDao).execute(note);
    }

    private class insertAsyncTask extends AsyncTask<NoteEntity, Void, Void> {
        private NoteDao dao;

        public insertAsyncTask(NoteDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(NoteEntity... note) {
            dao.insert(note[0]);
            return null;
        }
    }

    private class deleteAsyncTask extends AsyncTask<NoteEntity, Void, Void> {
        private NoteDao dao;

        public deleteAsyncTask(NoteDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(NoteEntity... note) {
            dao.delete(note[0]);
            return null;
        }
    }

    private class updateAsyncTask extends AsyncTask<NoteEntity, Void, Void> {
        private NoteDao dao;

        public updateAsyncTask(NoteDao dao) {
            this.dao = dao;
        }

        @Override
        protected Void doInBackground(NoteEntity... note) {
            dao.update(note[0]);
            return null;
        }
    }

}
