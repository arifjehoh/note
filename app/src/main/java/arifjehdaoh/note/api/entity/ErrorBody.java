package arifjehdaoh.note.api.entity;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

public class ErrorBody {
    public final int errorCode;

    private ErrorBody(int errorCode) {
        this.errorCode = errorCode;
    }

    public static ErrorBody fromString(String errorBody) throws JSONException {
        if (errorBody == null) {
            throw new JSONException("The errorBody is null");
        }

        Object object = new JSONTokener(errorBody).nextValue();
        if (!(object instanceof JSONObject)) {
            throw new JSONException("The errorBody is not a json object:" + errorBody);
        }

        JSONObject errorObject = (JSONObject) new JSONTokener(errorBody).nextValue();

        int errorCode = errorObject.getInt("ErrorCode");

        return new ErrorBody(errorCode);
    }
}
