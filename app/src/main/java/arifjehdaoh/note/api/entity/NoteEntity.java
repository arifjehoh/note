package arifjehdaoh.note.api.entity;

import org.threeten.bp.OffsetDateTime;
import org.threeten.bp.ZoneId;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.List;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;
import arifjehdaoh.note.classes.Checklist;

@Entity
public class NoteEntity extends BaseObservable{
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;
    @Bindable
    private String title;
    @Bindable
    private String text;
    @Bindable
    private OffsetDateTime dateTime;
    @Bindable
    private List<Checklist> tasks;
    @Ignore final ZoneId zoneId = ZoneId.systemDefault();
    @Ignore final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(zoneId);
    @Ignore final DateTimeFormatter timeFormatter = DateTimeFormatter.ofPattern("HH:mm").withZone(zoneId);

    public NoteEntity(int id, String title, String text, OffsetDateTime dateTime, List<Checklist> tasks) {
        this.id = id;
        this.title = title;
        this.text = text;
        this.dateTime = dateTime;
        this.tasks = tasks;
    }

    @Ignore
    public NoteEntity() {
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDateTime(OffsetDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setTasks(List<Checklist> tasks) {
        this.tasks = tasks;
    }

    public int getId() {
        return id;
    }

    public List<Checklist> getTasks() {
        return tasks;
    }

    public OffsetDateTime getDateTime() {
        return dateTime;
    }

    public String getText() {
        return text;
    }

    public String getTitle() {
        return title;
    }

    @Ignore public String getDate() {
        return dateTime.format(dateFormatter);
    }

    @Ignore public String getTime() {
        return dateTime.format(timeFormatter);
    }

}
