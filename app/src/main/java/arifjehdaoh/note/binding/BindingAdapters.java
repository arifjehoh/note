package arifjehdaoh.note.binding;

import android.graphics.Color;
import android.view.View;
import android.widget.EditText;

import androidx.databinding.BindingAdapter;
import arifjehdaoh.note.R;

public class BindingAdapters {

    @BindingAdapter("visibleGone")
    public static void setVisibleOrGone(View view, boolean show) {
        view.setVisibility(show ? View.VISIBLE : View.GONE);
    }
}
