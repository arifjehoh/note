package arifjehdaoh.note.noteadd;

import javax.inject.Inject;

import androidx.lifecycle.ViewModel;
import arifjehdaoh.note.api.entity.NoteEntity;
import arifjehdaoh.note.repository.NoteLocalDbRepository;

public class AddViewModel extends ViewModel {
    private NoteLocalDbRepository noteLocalDbRepository;

    @Inject
    public AddViewModel(NoteLocalDbRepository noteLocalDbRepository) {
        this.noteLocalDbRepository = noteLocalDbRepository;
    }

    public void insertNote(NoteEntity entity) {
        noteLocalDbRepository.insertNote(entity);
    }
}
