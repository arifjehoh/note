package arifjehdaoh.note.noteadd;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

import org.threeten.bp.OffsetDateTime;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import arifjehdaoh.note.BaseActivity;
import arifjehdaoh.note.R;
import arifjehdaoh.note.api.entity.NoteEntity;
import arifjehdaoh.note.checklist.ChecklistAdapter;
import arifjehdaoh.note.classes.Checklist;
import arifjehdaoh.note.databinding.ActivityAddBinding;
import timber.log.Timber;

import static arifjehdaoh.note.checklist.ChecklistAdapter.*;

public class AddActivity extends BaseActivity implements OnItemClickListener {
    private ActivityAddBinding mBinding;
    private AddViewModel mViewModel;
    private Checklist checklist;
    private InputMethodManager inputMethodManager;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_add);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(AddViewModel.class);

        inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);

        checklistSetUp();

        mBinding.setNote(new NoteEntity());
        mBinding.text.requestFocus();

        showKeyboard();

        mBinding.setOnAddBox(v -> {
            mBinding.getAdapter().checklistItem.add(new Checklist());
            mBinding.getAdapter().notifyItemInserted(mBinding.getAdapter().getItemCount());
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new RecyclerViewSwipe());//RecyclerRegistrationViewSwipe(binding.getAdapter(),this));
            itemTouchHelper.attachToRecyclerView(findViewById(R.id.recycler_view));

        });


    }

    private void checklistSetUp() {
        List<Checklist> checklistList = new ArrayList<>();
        ChecklistAdapter adapter = new ChecklistAdapter(checklistList, this);
        mBinding.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                hideKeyboard();
                finish();
                return true;
            case R.id.save:
                hideKeyboard();
                mBinding.getNote().setDateTime(OffsetDateTime.now());
                mBinding.getNote().setTasks(mBinding.getAdapter().checklistItem);
                if (mBinding.getNote().getTitle() != null && mBinding.getNote().getText() != null) {
                    mViewModel.insertNote(mBinding.getNote());
                    finish();
                }
                else if (mBinding.getNote().getText() != null) {
                    String[] title = mBinding.getNote().getText().split(" ");
                    mBinding.getNote().setTitle(title[0]);
                    mViewModel.insertNote(mBinding.getNote());
                    finish();
                } else {
                    Toast.makeText(this, getText(R.string.toast_text_no_input), Toast.LENGTH_SHORT).show();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showKeyboard() {
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    private void hideKeyboard() {
        inputMethodManager.hideSoftInputFromWindow(mBinding.text.getWindowToken(), 0);
    }

    @Override
    public void onItemClick(int position) {
        Timber.v("Checked %s", mBinding.getAdapter().checklistItem.get(position).isChecked());
    }

    private class RecyclerViewSwipe extends ItemTouchHelper.SimpleCallback {
        public RecyclerViewSwipe() {
            super(0, ItemTouchHelper.RIGHT);
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            int position = viewHolder.getAdapterPosition();
            deleteItem(position, getCurrentFocus());
        }

        @Override
        public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            View itemView = viewHolder.itemView;
            ColorDrawable background = new ColorDrawable(Color.RED);

            if (dX > 0) {
                background.setBounds(itemView.getLeft(), itemView.getTop(), itemView.getLeft()+((int) dX)+20,itemView.getBottom());
            } else {
                background.setBounds(0,0,0,0);
            }
            background.draw(c);
        }

        public void deleteItem(int position, View view) {
            checklist = mBinding.getAdapter().checklistItem.get(position);
            mBinding.getAdapter().checklistItem.remove(position);
            mBinding.getAdapter().notifyItemRemoved(position);
            mBinding.getNote().setTasks(mBinding.getAdapter().checklistItem);
            showUndoSnackbar(position, view);

        }

        private void showUndoSnackbar(int position, View view) {
            Snackbar snackbar = Snackbar.make(findViewById(R.id.content), R.string.snack_bar_undo_text_registration, Snackbar.LENGTH_SHORT);
            snackbar.setActionTextColor(Color.WHITE);

            snackbar.setAction(R.string.snack_bar_undo, v -> undoDelete(position));
            snackbar.show();
        }

        private void undoDelete(int position) {
            mBinding.getAdapter().checklistItem.add(position, checklist);
            mBinding.getNote().setTasks(mBinding.getAdapter().checklistItem);
            mBinding.getAdapter().notifyItemInserted(position);
        }
    }

}

