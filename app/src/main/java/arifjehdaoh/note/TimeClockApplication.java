package arifjehdaoh.note;

import com.jakewharton.threetenabp.AndroidThreeTen;

import dagger.android.AndroidInjector;
import dagger.android.support.DaggerApplication;

import timber.log.Timber;

import arifjehdaoh.note.di.ApplicationModule;
import arifjehdaoh.note.di.DaggerApplicationComponent;

public class TimeClockApplication extends DaggerApplication {

    @Override
    public void onCreate() {
        super.onCreate();

        AndroidThreeTen.init(this);
        setupTimber();
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        return DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .create(this);
    }

    private void setupTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }
}