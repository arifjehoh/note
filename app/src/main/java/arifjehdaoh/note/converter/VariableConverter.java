package arifjehdaoh.note.converter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.threeten.bp.OffsetDateTime;

import java.lang.reflect.Type;
import java.util.List;

import androidx.room.TypeConverter;
import arifjehdaoh.note.classes.Checklist;

public class VariableConverter {

    @TypeConverter
    public OffsetDateTime dateTimeFromDatabase(String value) {
        return OffsetDateTime.parse(value);
    }
    @TypeConverter
    public String dateTimeToDatabase(OffsetDateTime value) {
        return value.toString();
    }

    @TypeConverter
    public String listItemToDatabase(List<Checklist> values) {
        return new Gson().toJson(values);
    }

    @TypeConverter
    public List<Checklist> listItemFromDatabase(String value) {
        Type listType = new TypeToken<List<Checklist>>() {}.getType();
        return new Gson().fromJson(value, listType);
    }
}
