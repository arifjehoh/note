package arifjehdaoh.note.main;

import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;

import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import arifjehdaoh.note.BaseActivity;
import arifjehdaoh.note.R;
import arifjehdaoh.note.api.entity.NoteEntity;
import arifjehdaoh.note.databinding.ActivityMainBinding;
import arifjehdaoh.note.noteadd.AddActivity;
import arifjehdaoh.note.noteview.NoteActivtiy;

import static arifjehdaoh.note.main.NoteAdapter.*;

public class MainActivity extends BaseActivity  implements OnItemClickListener{
    public static final String NOTE_ID = "id";
    private ActivityMainBinding mBinding;
    private MainViewModel mViewModel;
    private NoteEntity note;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(MainViewModel.class);
        //mBinding.setIsThereNote(true);

        showNotesFromDatbase();

        mBinding.setOnFABClick(v -> {
            startActivity(new Intent(MainActivity.this, AddActivity.class));
        });
    }

    private void showNotesFromDatbase() {
        mViewModel
                .getNotes()
                .observe(this, noteEntities -> {
                    NoteAdapter adapter = new NoteAdapter(noteEntities, this);
                    mBinding.setAdapter(adapter);

                    isNotesEmpty(mBinding.getAdapter());

                    ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new RecyclerViewSwipe());//RecyclerRegistrationViewSwipe(binding.getAdapter(),this));
                    itemTouchHelper.attachToRecyclerView(findViewById(R.id.recycler_view));
                });
    }

    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(MainActivity.this, NoteActivtiy.class);
        Bundle bundle = new Bundle();
        bundle.putInt(NOTE_ID, mBinding.getAdapter().notes.get(position).getId());
        intent.putExtras(bundle);
        startActivity(intent);
    }


    private class RecyclerViewSwipe extends ItemTouchHelper.SimpleCallback {
        public RecyclerViewSwipe() {
            super(0, ItemTouchHelper.RIGHT);
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            int position = viewHolder.getAdapterPosition();
            deleteItem(position, getCurrentFocus());
        }

        @Override
        public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            View itemView = viewHolder.itemView;
            ColorDrawable background = new ColorDrawable(Color.RED);

            if (dX > 0) {
                background.setBounds(itemView.getLeft(), itemView.getTop(), itemView.getLeft()+((int) dX)+20,itemView.getBottom());
            } else {
                background.setBounds(0,0,0,0);
            }
            background.draw(c);
        }

        public void deleteItem(int position, View view) {
            note = mBinding.getAdapter().notes.get(position);
            mBinding.getAdapter().notes.remove(position);
            mBinding.getAdapter().notifyItemRemoved(position);
            mViewModel.delete(note);
            showUndoSnackbar(position, view);
            isNotesEmpty(mBinding.getAdapter());

        }

        private void showUndoSnackbar(int position, View view) {
            Snackbar snackbar = Snackbar.make(findViewById(R.id.content), R.string.snack_bar_undo_text_registration, Snackbar.LENGTH_SHORT);
            snackbar.setActionTextColor(Color.WHITE);

            snackbar.setAction(R.string.snack_bar_undo, v -> undoDelete(position));
            snackbar.show();
        }

        private void undoDelete(int position) {
            mBinding.getAdapter().notes.add(position, note);
            mViewModel.insertNote(note);
            mBinding.getAdapter().notifyItemInserted(position);
            isNotesEmpty(mBinding.getAdapter());
        }
    }
    private void isNotesEmpty(NoteAdapter adapter){
        if (adapter.notes.isEmpty()) {
            mBinding.setIsThereNote(true);
        } else {
            mBinding.setIsThereNote(false);
        }
    }
}
