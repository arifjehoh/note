package arifjehdaoh.note.main;

import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import arifjehdaoh.note.api.entity.NoteEntity;
import arifjehdaoh.note.repository.NoteLocalDbRepository;

public class MainViewModel extends ViewModel {
    private LiveData<List<NoteEntity>> notes;
    private NoteLocalDbRepository noteLocalDbRepository;

    @Inject
    public MainViewModel(NoteLocalDbRepository noteLocalDbRepository) {
        notes = noteLocalDbRepository.getNotes();
        this.noteLocalDbRepository = noteLocalDbRepository;
    }

    public LiveData<List<NoteEntity>> getNotes() {
        return notes;
    }

    public void delete(NoteEntity note) {
        noteLocalDbRepository.delete(note);
    }

    public  void insertNote(NoteEntity note) {
        noteLocalDbRepository.insertNote(note);
    }


}