package arifjehdaoh.note.main;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;
import arifjehdaoh.note.api.entity.NoteEntity;
import arifjehdaoh.note.converter.VariableConverter;

@Database(entities = NoteEntity.class, version = NoteDatabase.VERSION, exportSchema = false)
@TypeConverters({VariableConverter.class})
public abstract class NoteDatabase extends RoomDatabase {
    public static final int VERSION = 2;
    public abstract NoteDao noteDao();
}
