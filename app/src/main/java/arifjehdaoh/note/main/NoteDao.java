package arifjehdaoh.note.main;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import arifjehdaoh.note.api.entity.NoteEntity;

@Dao
public interface NoteDao {

    @Query("SELECT * FROM NoteEntity")
    LiveData<List<NoteEntity>> getNotes();

    @Query("SELECT * FROM NoteEntity WHERE id LIKE :id")
    LiveData<NoteEntity> getNote(int id);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(NoteEntity note);

    @Delete
    void delete(NoteEntity note);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void update(NoteEntity note);
}
