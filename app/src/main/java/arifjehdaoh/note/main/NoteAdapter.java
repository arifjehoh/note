package arifjehdaoh.note.main;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import arifjehdaoh.note.R;
import arifjehdaoh.note.api.entity.NoteEntity;
import arifjehdaoh.note.databinding.RecyclerViewNoteRowBinding;

import static androidx.databinding.DataBindingUtil.inflate;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.ViewHolder> {
    List<NoteEntity> notes;
    OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public NoteAdapter(List<NoteEntity> notes, OnItemClickListener onItemClickListener) {
        this.notes = notes;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RecyclerViewNoteRowBinding binding = inflate(inflater, R.layout.recycler_view_note_row,parent,false);
        return new ViewHolder(binding, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(notes.get(position));
    }

    @Override
    public int getItemCount() {
        return notes.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RecyclerViewNoteRowBinding mBinding;
        public ViewHolder(RecyclerViewNoteRowBinding binding, OnItemClickListener onItemClickListener) {
            super(binding.getRoot());
            this.mBinding = binding;

            itemView.setOnClickListener(v -> onItemClickListener.onItemClick(getAdapterPosition()));
        }
        public void bind(NoteEntity note) {
            mBinding.setNote(note);
        }
    }
}
