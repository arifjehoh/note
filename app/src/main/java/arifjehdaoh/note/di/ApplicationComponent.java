package arifjehdaoh.note.di;

import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

import javax.inject.Singleton;

import arifjehdaoh.note.TimeClockApplication;

@Component(modules = {
        AndroidSupportInjectionModule.class,
        ApplicationModule.class,
        NetworkModule.class,
        RoomModule.class,
        ActivityBuilderModule.class})
@Singleton
//@Singleton means that this components modules will contain singleton scoped or unscoped dependencies
public interface ApplicationComponent extends AndroidInjector<TimeClockApplication> {

    @Component.Builder
    abstract class Builder extends AndroidInjector.Builder<TimeClockApplication> {
        //public abstract ApplicationComponent build();
        public abstract Builder applicationModule(ApplicationModule applicationModule);
    }
}
