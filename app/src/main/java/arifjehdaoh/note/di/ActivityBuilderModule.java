package arifjehdaoh.note.di;

import arifjehdaoh.note.noteadd.AddActivity;
import arifjehdaoh.note.noteview.NoteActivtiy;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

import arifjehdaoh.note.main.MainActivity;

@Module
public abstract class ActivityBuilderModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = {FragmentBuilderModule.class})
    abstract MainActivity contributeMainActivityInjector();

    @ActivityScope
    @ContributesAndroidInjector(modules = {FragmentBuilderModule.class})
    abstract NoteActivtiy contributeNoteActivityInjector();

    @ActivityScope
    @ContributesAndroidInjector(modules = {FragmentBuilderModule.class})
    abstract AddActivity contributeAddActivityInjector();
}
