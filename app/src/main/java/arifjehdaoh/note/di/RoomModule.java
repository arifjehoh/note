package arifjehdaoh.note.di;

import android.content.Context;

import javax.inject.Singleton;

import androidx.room.Room;
import arifjehdaoh.note.main.NoteDao;
import arifjehdaoh.note.main.NoteDatabase;
import dagger.Module;
import dagger.Provides;

@Module
public class RoomModule {

    @Provides
    @Singleton
    public NoteDatabase provideNoteDatabase(Context context) {
        return Room.databaseBuilder(context,
                NoteDatabase.class,
                "Note.db")
                .fallbackToDestructiveMigration()
                .build();
    }

    @Provides
    @Singleton
    public NoteDao provideNoteDao(NoteDatabase database) {
        return database.noteDao();
    }
}
