package arifjehdaoh.note.di;

import arifjehdaoh.note.noteadd.AddViewModel;
import arifjehdaoh.note.noteview.NoteViewModel;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import arifjehdaoh.note.main.MainViewModel;
import arifjehdaoh.note.viewmodel.TimeClockViewModelFactory;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel.class)
    abstract ViewModel bindMainViewModel(MainViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(NoteViewModel.class)
    abstract ViewModel bindNoteViewModel(NoteViewModel viewModel);

    @Binds
    @IntoMap
    @ViewModelKey(AddViewModel.class)
    abstract ViewModel bindAddViewModel(AddViewModel viewModel);


    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(TimeClockViewModelFactory factory);
}
