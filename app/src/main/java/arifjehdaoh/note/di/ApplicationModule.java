package arifjehdaoh.note.di;

import android.app.Application;
import android.content.Context;

import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

/**
 * This module contains provider methods for things that are used all over the app.
 */
@Module(includes = ViewModelModule.class)
public class ApplicationModule {
    /*
    If this class is declared as abstract, then no setter is generated so that we can provide an instance of this module.
    We need to provide an instance as this class has a state
    */
    private final Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    public Context provideContext() {
        return application;
    }
}