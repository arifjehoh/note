package arifjehdaoh.note.checklist;

import android.graphics.Color;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import arifjehdaoh.note.R;
import arifjehdaoh.note.classes.Checklist;
import arifjehdaoh.note.databinding.RecyclerViewChecklistRowBinding;
import timber.log.Timber;

import static androidx.databinding.DataBindingUtil.inflate;


public class ChecklistAdapter extends RecyclerView.Adapter<ChecklistAdapter.ViewHolder> {
    public List<Checklist> checklistItem;
    public RecyclerViewChecklistRowBinding binding;
    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public ChecklistAdapter(List<Checklist> checklistItem, OnItemClickListener onItemClickListener) {
        this.checklistItem = checklistItem;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        this.binding = inflate(inflater, R.layout.recycler_view_checklist_row, parent, false);
        return new ViewHolder(binding, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(checklistItem.get(position));
    }

    @Override
    public int getItemCount() {
        return checklistItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        RecyclerViewChecklistRowBinding mBinding;

        public ViewHolder(RecyclerViewChecklistRowBinding binding, OnItemClickListener onItemClickListener) {
            super(binding.getRoot());
            this.mBinding = binding;

            itemView.setOnClickListener(v -> onItemClickListener.onItemClick(getAdapterPosition()));
        }

        public void bind(Checklist checklist) {
            mBinding.setChecklist(checklist);
            if (checklist.getText() == null) {
                mBinding.edittextCheckbox.requestFocus();
            }
        }
    }
}
