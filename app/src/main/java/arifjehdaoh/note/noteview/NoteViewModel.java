package arifjehdaoh.note.noteview;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import arifjehdaoh.note.api.entity.NoteEntity;
import arifjehdaoh.note.repository.NoteLocalDbRepository;

public class NoteViewModel extends ViewModel {
    private NoteLocalDbRepository noteLocalDbRepository;

    @Inject
    public NoteViewModel(NoteLocalDbRepository noteLocalDbRepository) {
        this.noteLocalDbRepository = noteLocalDbRepository;
    }

    public LiveData<NoteEntity> getNote(int id) {
        return noteLocalDbRepository.getNote(id);
    }

    public void update(NoteEntity noteEntity) {
        noteLocalDbRepository.update(noteEntity);
    }
}
