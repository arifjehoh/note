package arifjehdaoh.note.noteview;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;
import arifjehdaoh.note.BaseActivity;
import arifjehdaoh.note.R;
import arifjehdaoh.note.checklist.ChecklistAdapter;
import arifjehdaoh.note.classes.Checklist;
import arifjehdaoh.note.databinding.ActivityNoteBinding;
import arifjehdaoh.note.main.MainActivity;

public class NoteActivtiy extends BaseActivity implements ChecklistAdapter.OnItemClickListener {
    public static int id;
    private ActivityNoteBinding mBinding;
    private NoteViewModel mViewModel;
    private Checklist checklist;
    private static Menu mMenu;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_note);
        mViewModel = ViewModelProviders.of(this, viewModelFactory).get(NoteViewModel.class);

        if (getIntent() == null) {
            finish();
        }
        id = getIntent().getExtras().getInt(MainActivity.NOTE_ID);
        showNote(getIntent().getExtras().getInt(MainActivity.NOTE_ID));



        mBinding.setOnAddBox(v -> {
            mBinding.getAdapter().checklistItem.add(new Checklist());
            mBinding.getAdapter().notifyItemInserted(mBinding.getAdapter().getItemCount());
        });
    }

    private void showNote(int id) {
        mViewModel
                .getNote(id)
                .observe(this, noteEntity -> {
                    mBinding.setNote(noteEntity);
                    if (noteEntity.getTasks() == null) {
                        return;
                    }
                    ChecklistAdapter adapter = new ChecklistAdapter(noteEntity.getTasks(), this);
                    mBinding.setAdapter(adapter);


                    ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new RecyclerViewSwipe());//RecyclerRegistrationViewSwipe(binding.getAdapter(),this));
                    itemTouchHelper.attachToRecyclerView(findViewById(R.id.recycler_view));
                });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.edit_menu, menu);
        mMenu = menu;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mBinding.getNote().setTasks(mBinding.getAdapter().checklistItem);
                mViewModel.update(mBinding.getNote());
                finish();
                return true;
            case R.id.edit:
                mBinding.setEditable(!mBinding.getEditable());
                item.setVisible(false);
                mMenu.findItem(R.id.save).setVisible(true);
                break;
            case R.id.save:
                item.setVisible(false);
                mMenu.findItem(R.id.edit).setVisible(true);
                mBinding.getNote().setTasks(mBinding.getAdapter().checklistItem);
                mViewModel.update(mBinding.getNote());
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(int position) {

    }

    private class RecyclerViewSwipe extends ItemTouchHelper.SimpleCallback {
        public RecyclerViewSwipe() {
            super(0, ItemTouchHelper.RIGHT);
        }

        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            int position = viewHolder.getAdapterPosition();
            deleteItem(position, getCurrentFocus());
        }

        @Override
        public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            View itemView = viewHolder.itemView;
            ColorDrawable background = new ColorDrawable(Color.RED);

            if (dX > 0) {
                background.setBounds(itemView.getLeft(), itemView.getTop(), itemView.getLeft()+((int) dX)+20,itemView.getBottom());
            } else {
                background.setBounds(0,0,0,0);
            }
            background.draw(c);
        }

        public void deleteItem(int position, View view) {
            checklist = mBinding.getAdapter().checklistItem.get(position);
            mBinding.getAdapter().checklistItem.remove(position);
            mBinding.getAdapter().notifyItemRemoved(position);
            mBinding.getNote().setTasks(mBinding.getAdapter().checklistItem);
            showUndoSnackbar(position, view);

        }

        private void showUndoSnackbar(int position, View view) {
            Snackbar snackbar = Snackbar.make(findViewById(R.id.content), R.string.snack_bar_undo_text_registration, Snackbar.LENGTH_SHORT);
            snackbar.setActionTextColor(Color.WHITE);

            snackbar.setAction(R.string.snack_bar_undo, v -> undoDelete(position));
            snackbar.show();
        }

        private void undoDelete(int position) {
            mBinding.getAdapter().checklistItem.add(position, checklist);
            mBinding.getNote().setTasks(mBinding.getAdapter().checklistItem);
            mBinding.getAdapter().notifyItemInserted(position);
        }
    }

}
